import json

from django.http import JsonResponse


def index(request):
    headers = str(request.headers).replace("'", '"')
    body = request.body.decode()
    if request.method == 'GET':
        data = json.dumps(request.GET)
        response = ({"status": "ok",
                     "method": request.method,
                     "headers": headers,
                     "body": body,
                     "data": data})
    elif request.method == 'POST':
        data = json.dumps(request.POST)
        response = ({"status": "ok",
                     "method": request.method,
                     "headers": headers,
                     "body": body,
                     "data": data})
    else:
        response = ({"status": "error", "method": "Invalid request"})
    # Write response
    text_file = open("dum-server-output.json", "a")
    text_file.write(str(response))
    text_file.close()
    # Send response
    return JsonResponse(response)
